#!/usr/bin/env python
import csv


def create_samplesheet_from_sampleinfo(fn, fn_out, serie):
    header = "Sample_ID,Sample_Name,Description,I7_Index_ID,index,I5_Index_ID,index2,Sample_Project,Sample_Plate,Sample_Well"
    with open(fn) as f, open(fn_out, 'w') as f_out:
        f_out.write('{}\n'.format(header))
        reader = csv.reader(f)
        _header = next(reader)
        cnv_info = list()
        patient_info = list()
        for line in reader:
            _serie, sample, material, sex, request, dob, deadline = line
            f_out.write('{},{},CTD_NGS,i7,i7,i5,i5,{},,\n'.format(sample, sample, serie))
            

if __name__ == '__main__':
    import os
    import glob
    import json
    import argparse

    from ngsscriptlibrary import samplesheet_to_sample_genesis
    from ngsscriptlibrary import run_command
    from ngsscriptlibrary import read_config_json

    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--serie", type=str, 
                        help="Miseq serie nummer", required=True)
    parser.add_argument("--samplesheet", type=str,
                        help="Sample sheet (afwijkend van BaseSpace)")
    parser.add_argument("-t", "--threads", type=int, default=10,
                        help="Number of threads for snakemake")
    parser.add_argument("-p", "--pipeline", action='store_true',
                        help="Only run the pipeline")
    parser.add_argument("--download", action='store_true',
                        help="Download reads from basespace")
    parser.add_argument("--cleanup", action='store_true',
                        help="Move data to archive")
    parser.add_argument("--unlock", action='store_true',
                        help="Unlock snakemake")
    parser.add_argument("--notemp", action='store_true',
                        help="Do not remove temporary files")                                                 

    args = parser.parse_args()

    serie = args.serie
    threads = args.threads

    if not args.download and not args.pipeline and not args.cleanup:
        args.download = True
        args.pipeline = True
        args.cleanup = True

    BASEDIR = os.path.join('/', 'data', 'dnadiag', 'reads')
    ADDINFO = os.path.join('/', 'data', 'dnadiag', 'ngsdump', 'ctd_sample_info_parser.py')
    ARCHIEF = os.path.join('/', 'mnt', 'kg_nextgen_archief', 'CTD_output')
    SCRIPTHOME = os.path.join('/', 'data', 'dnadiag', 'ctdpipeline')
    CONFIG = os.path.join(SCRIPTHOME, 'ctdpipeline', 'config.yaml')
    CONFIG_JSON = os.path.join(SCRIPTHOME, 'ctdpipeline', 'config.json')
    
    READS = os.path.join('/', 'mnt', 'VUMC', 'VUMC', 'Incoming', 'Spoed', 'CTD', '*_{}'.format(serie))
    READS = glob.glob(READS)[0]
    SAMPLEINFO = glob.glob('{}/*verzicht*{}.csv'.format(READS, serie))[0]
    SERIEDIR = os.path.join(BASEDIR, '{}'.format(serie))
    OUTPUTDIR = os.path.join('/', 'mnt', 'VUMC', 'VUMC', 'Outgoing', '{}'.format(serie))

    config_dict = read_config_json(CONFIG_JSON)
    TARGETDB = os.path.join(config_dict["TARGET"], 'varia', 'captures.sqlite') 
    
    if args.samplesheet:
        SAMPLESHEET = args.samplesheet
    else:
        SAMPLESHEET = os.path.join(READS, 'SampleSheet.csv')

    if args.download:
        
        os.mkdir(SERIEDIR)
        create_samplesheet_from_sampleinfo(SAMPLEINFO, SAMPLESHEET, serie)
        run_command('cp -r {}/*.gz {}'.format(READS, SERIEDIR))
        run_command('cp {} {}/SampleInfo.csv'.format(SAMPLEINFO, SERIEDIR))
        run_command('cp {} {}/'.format(SAMPLESHEET, SERIEDIR))

    if args.pipeline:

        run_command('python3 {} --input {}/SampleInfo.csv'.format(ADDINFO, SERIEDIR))
        samples = [_[0] for _ in samplesheet_to_sample_genesis('{}/SampleSheet.csv'.format(SERIEDIR))]
        if not os.path.isdir(os.path.join(SERIEDIR, 'SNPcheck')):
            os.mkdir(os.path.join(SERIEDIR, 'SNPcheck'))
        for sample in samples:
            snpcheckfile = os.path.join(SERIEDIR, 'SNPcheck', '{}.qpcrsnpcheck'.format(sample))
            if not os.path.isfile(snpcheckfile):
                run_command('touch {}'.format(snpcheckfile))
        
        run_snakemake = 'snakemake --rerun-incomplete -s {}/ctdpipeline/ctdpipeline.snakefile -j{} --configfile {} --directory {}'.format(
            SCRIPTHOME, threads, CONFIG, SERIEDIR )
        
        if args.unlock:
            run_snakemake = f'{run_snakemake} --unlock'
        if args.notemp:
            run_snakemake = f'{run_snakemake} --notemp'

        run_command(run_snakemake)

    if args.cleanup:
        try:
            os.mkdir('{}'.format(OUTPUTDIR))
        except FileExistsError:
            print('{} bestaat al. Verwijder {} om opnieuw te kopieren.'.format(
                OUTPUTDIR, OUTPUTDIR))
        else:
            run_command('cp -r {}/output/* {}'.format(SERIEDIR, OUTPUTDIR))

        try:
            os.mkdir('{}/{}'.format(ARCHIEF, serie))
        except FileExistsError:
            print('{}/{} bestaat al. Verwijder {}/{} om opnieuw te kopieren.'.format(
                ARCHIEF, serie, ARCHIEF, serie))
        else:
            run_command('cp -r {}/output/CNV_CTD* {}/output/*.xlsx {}/output/*.pdf {}/SampleSheet.csv {}/SampleInfo.csv {}/input.json {}/{}'.format(
                SERIEDIR, SERIEDIR, SERIEDIR, SERIEDIR, SERIEDIR, SERIEDIR, ARCHIEF, serie))
        run_command("rsync -rlzuvP /data/dnadiag/databases/ /mnt/kg_nextgen_archief/ServerDatabaseArchief/")
